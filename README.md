# cppc2020

* This folder contains the example programs used in the TDT4102 course lectured at NTNU - Trondheim in Norway.
* The course is given from January to April year 2021, for approximately 1150 students.  We are using the PPP book (2nd edition) by Bjarne Stroustrup, but not all of that book is covered by the course.
* Comments are appreciated, and should be e-mailed to xxx-fagans@idi.ntnu.no, where xxx is the course code (see the first bullet point). 

Best regards
_Lasse Natvig_
NTNU - Trondheim, Norway
[https://www.ntnu.edu/employees/lasse.natvig]

#### Additional modifications:
Bart Iver van Blokland
NTNU - Trondheim, Norway
[https://www.ntnu.edu/employees/bartiver]
