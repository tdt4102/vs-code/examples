 // How to read and use arguments that have been given through the command line

#include <iostream>
#include <string>
#include <vector>

using namespace std;

// The main function is usually defined with the argc and argv arguments.
// The argc (ARGument Count) parameter is the number of command line arguments that were given to the program
// The argv (ARGument Vector) parameter is an array of C-style strings, each representing one parameter
// Note: the first parameter is always the path to the program executable
int main(int argc, char* argv[]) {

    // Our program here will take two arguments.
    // We first check with the argc variable to ensure we have received that exact number
    // Remember that the first argument is the name of the program executable. 
    // Since our program takes two arguments, we need to check if argv contains 2 + 1 = _three_ arguments.
    if (argc != 3) {
        // Since we like to be helpful, we'll tell the user to provide two arguments if they were not provided
        // we can use the standard error (cerr) stream for this.
        cerr << "This program requires two arguments, but instead " << (argc - 1) << " were provided." << endl;
		cerr << "Usage: " << argv[0] << " argument_1 argument_2" << endl;
		return 1;
	}

    // While not technically required, we should convert the arguments into regular strings rather than C strings before proceeding.
	vector <string> myArgs;
	for (int i = 0; i < argc; ++i) {
		myArgs.push_back(argv[i]);
	}

    // We can now print the contents of the argv array
	for (int i = 0; i < myArgs.size(); i++) {
		cout << "The argument with index " << i << " in argv is: " << myArgs.at(i) << endl;
	}

    // And use them for something
    cout << "I love " << myArgs.at(1) << ", but really dislike " << myArgs.at(2) << "." << endl;

	return 0;
}
