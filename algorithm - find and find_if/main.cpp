#include <iostream>
#include <vector>
#include <set>
#include <algorithm> // Required by find_if() (But not by find())

using namespace std;

// A function which prints out whether a given vector contains a particular value
void searchAndPrint(vector<int>& vectorToSearch, int valueToFind) {
	vector<int>::iterator iterator = find(vectorToSearch.begin(), vectorToSearch.end(), valueToFind);
	if (iterator != vectorToSearch.end()) {
		cout << "The value " << *iterator << " was found!" << endl;
	} else {
        cout << "The value " << valueToFind << " was not found :(" << endl;
    }
}

// The exact same function as before, except this time any container of any datatype can be inserted
template <typename T, typename E>
void genericSearchAndPrint(const T& container, const E& valueToFind) {
	auto iterator = find(container.begin(), container.end(), valueToFind);	
	if (iterator != container.end()) {
		cout << "The value " << *iterator << " was found!" << endl;
	} else {
        cout << "The value " << *iterator << " was not found :(" << endl;
    }
}

// These three functions and class are used in the find_if examples below
bool isOdd(int value) { return (value % 2) == 1; }

bool larger_than_42(double x) { return x > 42; }

double someValue = 0; 
bool larger_than_someValue(double x) { return x > someValue; }

class Larger_than {
	double someValue;
public:
	Larger_than(double value) : someValue{ value } {}
	bool operator()(int x) const { return x > someValue; }
};

int main() {
	vector<int> numberVector{ 11, 22, 33, 77, 44 };
    set<double> numberSet{ 11.11, 22.22, 44.44, 33.33 };

	searchAndPrint(numberVector, 22);
	searchAndPrint(numberVector, 23);
	searchAndPrint(numberVector, 44);

	genericSearchAndPrint(numberVector, 77);
	genericSearchAndPrint(numberSet, 22.23);
	genericSearchAndPrint(numberSet, 22.22);

	vector<int> anotherNumberVector{ 10, 22, 33, 77, 44 };

	auto iterator = find_if(anotherNumberVector.begin(), anotherNumberVector.end(), isOdd);
	if (iterator != anotherNumberVector.end()) {
		cout << "The first odd number in the vector is " << *iterator << endl;
	}

	vector<double> doubleVector{ 11.11, 22.22, 44.44, 55.55, 33.33 };
	auto iterator2 = find_if(doubleVector.begin(), doubleVector.end(), larger_than_42);
	if (iterator2 != doubleVector.end()) {
		cout << "The first number in the vector larger than 42 is " << *iterator2 << endl;
	}

    // Note that the larger_than_someValue function up above compares to the value of the someValue variable.
	someValue = 42.0;
	auto iterator3 = find_if(doubleVector.begin(), doubleVector.end(), larger_than_someValue);
	if (iterator3 != doubleVector.end()) {
		cout << "The first number in the vector larger than the current value of the someValue variable is " << *iterator3 << endl;
	}

    // This example uses the Larger_than class as a means to determine what to find
	auto iterator4 = find_if(doubleVector.begin(), doubleVector.end(), Larger_than(42.0));
	if (iterator4 != doubleVector.end()) {
		cout << "The first number in the vector that was larger than the one passed into the Larger_than class is " << *iterator4 << endl;
	}

	return 0;
}