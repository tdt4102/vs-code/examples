// Algorithm - accumulate inner_product partial_sum sort

#include <iostream>
#include <vector>
#include <numeric>
#include <algorithm>
#include <string>

using namespace std;

struct Student {
	string name = "";
	double grade = 0;
	Student(string studentName, double studentGrade) :
		name{ studentName }, grade{ studentGrade } {}
};

// Function for comparing two students, used for sorting later
bool operator<(const Student& lhs, const Student& rhs) {
	return lhs.grade > rhs.grade;
}

// Basically the same function, but for sorting students by name
bool compareStudentNames(const Student& lhs, const Student& rhs) {
	return lhs.name < rhs.name;
}

// Allows printing of Student structs using cout
ostream& operator<<(ostream& ofs, const Student& s) {
	return ofs << s.name << " : " << s.grade;
}

int main() {
	{ 	// Part 1:  (These are from the <numeric> include file)

        // The accumulate algorithm adds all items together in a container, with a given start value.
        // In this case, the values 1, 1, 1, and 1000 are added to the starting value of 3.
        // 3 + 1 + 1 + 1 + 1000 = 1006
		vector<int> numberList {1, 1, 1, 1000};
		cout << "Part 1: accumulate: " << accumulate(numberList.begin(), numberList.end(), 3) << endl;

        // The inner_product function is similar, except it sums the product of elements of two containers.
        // Also note that this one too takes a starting value parameter
        // 100 + (1 * 2) + (1 * 0) + (1 * 0) + (1000 * 3) = 3102 
		vector<int> anotherNumberList {2, 0, 0, 3};
		cout << "Part 1: inner_product: " << inner_product(numberList.begin(), numberList.end(), anotherNumberList.begin(), 100) << endl;

        // The partial_sum function computes the sum up to that point, and writes it to an output container
        // In this case: 
        // 0: 1                 = 1
        // 1: 1 + 1             = 2
        // 2: 1 + 1 + 1         = 3
        // 3: 1 + 1 + 1 + 1000  = 1003
		vector<int> result(numberList.size());
		partial_sum(numberList.begin(), numberList.end(), result.begin());
		cout << "Part 1: partial_sum: ";
        for (int e : result) {
            cout << e << " ";
        }
        cout << endl;
	}
    cout << endl;

	{ 	// Part 2: The sort algorithm
        // The sort algorithm takes a container and sorts its contents
		vector<double> doubleVector{ 22.22, 11.11, 33.33, 7.7 };
		sort(doubleVector.begin(), doubleVector.end());
		cout << "Part 2: Basic usage of the sort algorithm: ";
        for (double element : doubleVector) {
            cout << element << " ";
        }
		cout << endl; 

		vector<Student> studentVector {{"Kari", 4.5}, {"Ola", 4.0}, {"Bjarne", 3.0}};

        // You can also sort data types you have created yourself. 
        // However, in these cases you need to define a function to compare them, which can be done in two ways.

        // The first way is by defining the "less than" operator
        // We have done so above: "bool operator<(const Student& lhs, const Student& rhs)"
		sort(studentVector.begin(), studentVector.end());
        cout << "Part 2: Sorting custom data types using operator<: ";
		for (Student student : studentVector) {
            // We are able to print our Student datatype here using cout because we defined the operator<< for it above
            cout << student << ", ";
        }
		cout << endl;

        // It is also possible to provide a function which compares two instances of your custom datatype by defining a so-called comparator
        // We have done so above: "bool compareStudentNames(const Student& lhs, const Student& rhs)"
        // As the name implies, this comparator sorts students by their name
		sort(studentVector.begin(), studentVector.end(), compareStudentNames);
		cout << "Part 2: Sorting custom data types using a comparator: ";
        for (Student student : studentVector) {
            cout << student << ", ";
        }
		cout << endl;
	}
    cout << endl;

	return 0;
}