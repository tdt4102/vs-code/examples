  /* Vector intro --- Demonstrates simple use of vector:
	- declaration and initialization
	- different ways to iterate through all stored elements
	- how to append new elements
	- size of vector
	- how to update an element in a vector
	- function returning a vector
	*/
#include "std_lib_facilities.h"

// This function is used in the main() function below
vector<double> roots() {
	vector <double> v;
	for (int i = 2; i < 11; i++) {
		v.push_back(sqrt(i));
	}
	return v;
}

void printVector(vector<int> numbers) {
	cout << "    ";
	for (unsigned int i = 0; i < numbers.size(); i++) { 
		// Using the at() function to retrieve an element from a vector will perform a check whether the requested index 
		// lies inside the vector. When instead using numbers[i] here, that check is not performed
		// It's generally a good idea to use the at() function as much as possible, as it helps you uncover problems in your code
		cout << numbers.at(i) << " ";  
	}
	// std::endl stands for END Line
	cout << endl;
}

void printVector(vector<double> realNumbers) {
	// The same as the function above.
	cout << "    ";
	for (unsigned int i = 0; i < realNumbers.size(); i++) { 
		cout << realNumbers.at(i) << " ";  
	}
	cout << endl;
}

int main() {
	// Let's start by creating a vector with some numbers in it
	vector<int> numbers {-3, 3, 5, 8888};

	// For starters, we'll print out the contents of this vector
	cout << "The initial contents of the numbers vector: " << endl;
	printVector(numbers);

	// We'll now add some new elements at the end of it
	// push_back() takes the value you pass in, and places it at the end of the vector
	numbers.push_back(42);
	numbers.push_back(1337);

	cout << "The numbers vector after two elements were appended to it:" << endl;
	printVector(numbers);

	// You can also use the at() function to change values in a vector
	numbers.at(0) = -4;

	cout << "The numbers vector after changing its first element:" << endl;
	printVector(numbers);

	// The size function tells you how many entries a vector has
	cout << "There are " << numbers.size() << " elements in the numbers vector." << endl;

	// It is possible to iterate over a vector using a range-based for loop (See PPP page 119)
	cout << "Printing the numbers vector using a range-based for loop:" << endl;
	cout << "    ";
	for (int number : numbers) {
		cout << number << " ";
	}
	cout << endl;



	// Vectors can contain (almost) any type. Here's one with double numbers
	vector<double> realNumbers; // initially empty
	realNumbers.push_back(3.0);
	realNumbers.push_back(pow(3.0, 2.0));
	realNumbers.push_back(sqrt(10.0));
	
	cout << "Printing the realNumbers vector:" << endl;
	printVector(realNumbers);

	// Vectors can also be returned from a function. 
	// Here's an example of that
	vector<double> rootsVector = roots();

	cout << "The results of the roots() function:" << endl;
	printVector(rootsVector);
	
	// You can also create a vector which already has a particular number of elements
	// In this case, this creates a vector with 30 integers:
	vector<int> longerVector(30);

	// note that this uses the () brackets, not the {} we used before

	cout << "A vector with 30 integers:" << endl;
	printVector(longerVector);
}